/*
package ru.t1.ktitov.tm.repository;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.ktitov.tm.api.repository.dto.ISessionRepositoryDTO;
import ru.t1.ktitov.tm.api.service.IConnectionService;
import ru.t1.ktitov.tm.api.service.IPropertyService;
import ru.t1.ktitov.tm.marker.UnitCategory;
import ru.t1.ktitov.tm.dto.model.SessionDTO;
import ru.t1.ktitov.tm.repository.dto.SessionRepositoryDTO;
import ru.t1.ktitov.tm.service.ConnectionService;
import ru.t1.ktitov.tm.service.PropertyService;

import javax.persistence.EntityManager;
import java.util.List;

import static ru.t1.ktitov.tm.constant.SessionTestData.*;
import static ru.t1.ktitov.tm.constant.UserTestData.*;

@Category(UnitCategory.class)
public final class SessionRepositoryTest {

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final IConnectionService connectionService = new ConnectionService(propertyService);

    @NotNull
    protected EntityManager getEntityManager() {
        return connectionService.getEntityManager();
    }

    @NotNull
    protected ISessionRepositoryDTO getRepository(@NotNull EntityManager entityManager) {
        return new SessionRepositoryDTO(entityManager);
    }

    private void compareSessions(@NotNull final SessionDTO session1, @NotNull final SessionDTO session2) {
        Assert.assertEquals(session1.getId(), session2.getId());
        Assert.assertEquals(session1.getUserId(), session2.getUserId());
        Assert.assertEquals(session1.getRole(), session2.getRole());
    }

    private void compareSessions(
            @NotNull final List<SessionDTO> sessionList1,
            @NotNull final List<SessionDTO> sessionList2) {
        Assert.assertEquals(sessionList1.size(), sessionList2.size());
        for (int i = 0; i < sessionList1.size(); i++) {
            compareSessions(sessionList1.get(i), sessionList2.get(i));
        }
    }

    @After
    @SneakyThrows
    public void tearDown() {
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final ISessionRepositoryDTO repository = getRepository(entityManager);
            entityManager.getTransaction().begin();
            repository.clear(USER1_SESSION1.getId());
            repository.clear(USER1_SESSION2.getId());
            repository.clear(USER1_SESSION3.getId());
            repository.clear(USER2_SESSION1.getId());
            repository.clear(USER2_SESSION2.getId());
            repository.clear(USER2_SESSION3.getId());
            repository.clear(ADMIN1_SESSION1.getId());
            repository.clear(ADMIN1_SESSION2.getId());
            repository.clear(ADMIN1_SESSION3.getId());
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Test
    @SneakyThrows
    public void add() {
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final ISessionRepositoryDTO repository = getRepository(entityManager);
            entityManager.getTransaction().begin();
            Assert.assertTrue(repository.findAll().isEmpty());
            repository.add(USER1_SESSION1);
            compareSessions(USER1_SESSION1, repository.findAll().get(0));
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Test
    @SneakyThrows
    public void addList() {
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final ISessionRepositoryDTO repository = getRepository(entityManager);
            entityManager.getTransaction().begin();
            Assert.assertTrue(repository.findAll().isEmpty());
            repository.add(USER1_SESSION1);
            repository.add(USER1_SESSION2);
            repository.add(USER1_SESSION3);
            Assert.assertEquals(3, repository.getSize());
            compareSessions(USER1_SESSION_LIST, repository.findAll());
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Test
    @SneakyThrows
    public void clear() {
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final ISessionRepositoryDTO repository = getRepository(entityManager);
            entityManager.getTransaction().begin();
            repository.add(USER1_SESSION1);
            compareSessions(USER1_SESSION1, repository.findAll().get(0));
            repository.clear();
            Assert.assertTrue(repository.findAll().isEmpty());
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Test
    @SneakyThrows
    public void clearByUserId() {
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final ISessionRepositoryDTO repository = getRepository(entityManager);
            entityManager.getTransaction().begin();
            repository.add(USER1_SESSION1);
            repository.add(USER1_SESSION2);
            repository.add(USER1_SESSION3);
            compareSessions(USER1_SESSION_LIST, repository.findAll());
            repository.clear(USER2.getId());
            Assert.assertFalse(repository.findAll().isEmpty());
            repository.clear(USER1.getId());
            Assert.assertTrue(repository.findAll().isEmpty());
            repository.add(USER1_SESSION1);
            repository.add(USER2_SESSION1);
            repository.clear(USER1.getId());
            compareSessions(USER2_SESSION1, repository.findAll().get(0));
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Test
    @SneakyThrows
    public void findAll() {
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final ISessionRepositoryDTO repository = getRepository(entityManager);
            entityManager.getTransaction().begin();
            repository.add(USER1_SESSION1);
            repository.add(USER1_SESSION2);
            repository.add(USER1_SESSION3);
            repository.add(USER2_SESSION1);
            repository.add(USER2_SESSION2);
            repository.add(USER2_SESSION3);
            Assert.assertEquals(6, repository.getSize());
            compareSessions(USER1_SESSION_LIST, repository.findAll(USER1.getId()));
            compareSessions(USER2_SESSION_LIST, repository.findAll(USER2.getId()));
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Test
    @SneakyThrows
    public void findOneById() {
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final ISessionRepositoryDTO repository = getRepository(entityManager);
            entityManager.getTransaction().begin();
            repository.add(USER1_SESSION1);
            repository.add(USER1_SESSION2);
            repository.add(USER1_SESSION3);
            compareSessions(USER1_SESSION1, repository.findOneById(USER1_SESSION1.getId()));
            Assert.assertNull(repository.findOneById(USER2.getId(), USER1_SESSION1.getId()));
            compareSessions(USER1_SESSION1, repository.findOneById(USER1.getId(), USER1_SESSION1.getId()));
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Test
    @SneakyThrows
    public void remove() {
        @NotNull final EntityManager entityManager = getEntityManager();
        try {
            @NotNull final ISessionRepositoryDTO repository = getRepository(entityManager);
            entityManager.getTransaction().begin();
            repository.add(USER1_SESSION1);
            repository.add(USER1_SESSION2);
            repository.add(USER1_SESSION3);
            repository.remove(USER1_SESSION1);
            Assert.assertEquals(2, repository.getSize());
            repository.removeById(USER1_SESSION2.getId());
            Assert.assertEquals(1, repository.getSize());
            compareSessions(USER1_SESSION3, repository.findAll().get(0));
            repository.clear();
            repository.add(USER1_SESSION1);
            repository.add(USER1_SESSION2);
            repository.add(USER1_SESSION3);
            Assert.assertEquals(3, repository.getSize());
            repository.removeById(USER2.getId(), USER1_SESSION1.getId());
            Assert.assertEquals(3, repository.getSize());
            repository.removeById(USER1.getId(), USER1_SESSION1.getId());
            repository.removeById(USER1.getId(), USER1_SESSION2.getId());
            compareSessions(USER1_SESSION3, repository.findAll().get(0));
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

}
*/
