package ru.t1.ktitov.tm.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.ktitov.tm.api.repository.model.ISessionRepository;
import ru.t1.ktitov.tm.enumerated.Sort;
import ru.t1.ktitov.tm.model.Session;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;

public final class SessionRepository extends AbstractUserOwnedRepository<Session> implements ISessionRepository {

    public SessionRepository(@NotNull final EntityManager entityManager) {
        super(entityManager);
    }

    @NotNull
    @Override
    public List<Session> findAll() {
        @NotNull final String jpql = "SELECT m FROM Session m";
        return entityManager.createQuery(jpql, Session.class).getResultList();
    }

    @NotNull
    @Override
    public List<Session> findAll(@Nullable final String userId) {
        @NotNull final String jpql = "SELECT m FROM Session m WHERE user.id = :userId";
        return entityManager.createQuery(jpql, Session.class)
                .setParameter("userId", userId)
                .getResultList();
    }

    @Nullable
    @Override
    public List<Session> findAll(@Nullable final String userId, @Nullable final Sort sort) {
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Session> criteriaQuery = criteriaBuilder.createQuery(Session.class);
        Root<Session> session = criteriaQuery.from(Session.class);
        criteriaQuery.select(session)
                .where(criteriaBuilder.equal(session.get("userId"), userId))
                .orderBy(criteriaBuilder.asc(session.get(Sort.getOrderByField(sort))));
        TypedQuery<Session> typedQuery = entityManager.createQuery(criteriaQuery);
        return typedQuery.getResultList();
    }

    @Override
    public Session findOneById(@Nullable final String id) {
        return entityManager.find(Session.class, id);
    }

    @Override
    public Session findOneById(@Nullable final String userId, @Nullable final String id) {
        @NotNull final String jpql = "SELECT m FROM Session m WHERE id = :id AND user.id = :userId";
        return entityManager.createQuery(jpql, Session.class)
                .setParameter("id", id)
                .setParameter("userId", userId)
                .setMaxResults(1)
                .getResultList()
                .stream().findFirst().orElse(null);
    }

    @Override
    public boolean existsById(@Nullable final String id) {
        return findOneById(id) != null;
    }

    @Override
    public boolean existsById(@Nullable final String userId, @Nullable final String id) {
        return findOneById(userId, id) != null;
    }

    @Override
    public int getSize() {
        @NotNull final String jpql = "SELECT COUNT(m) FROM Session";
        return entityManager.createQuery(jpql, Session.class)
                .setMaxResults(1)
                .getFirstResult();
    }

    @Override
    public int getSize(@Nullable final String userId) {
        @NotNull final String jpql = "SELECT COUNT(m) FROM Session WHERE user.id = :userId";
        return entityManager.createQuery(jpql, Session.class)
                .setParameter("userId", userId)
                .setMaxResults(1)
                .getFirstResult();
    }

    @Override
    public void clear() {
        @Nullable final List<Session> sessions = findAll();
        if (!sessions.isEmpty())
            sessions.forEach(entityManager::remove);
    }

    @Override
    public void clear(@Nullable final String userId) {
        @NotNull final String jpql = "DELETE FROM Session m WHERE user.id = :userId";
        entityManager.createQuery(jpql)
                .setParameter("userId", userId)
                .executeUpdate();
    }

}
