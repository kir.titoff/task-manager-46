package ru.t1.ktitov.tm.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.ktitov.tm.api.repository.model.ITaskRepository;
import ru.t1.ktitov.tm.enumerated.Sort;
import ru.t1.ktitov.tm.model.Task;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.Collections;
import java.util.List;

public final class TaskRepository extends AbstractUserOwnedRepository<Task> implements ITaskRepository {

    public TaskRepository(@NotNull final EntityManager entityManager) {
        super(entityManager);
    }

    @NotNull
    @Override
    public List<Task> findAllByProjectId(
            @Nullable final String userId,
            @Nullable final String projectId
    ) {
        if (userId == null || userId.isEmpty()) return Collections.emptyList();
        if (projectId == null || projectId.isEmpty()) return Collections.emptyList();
        @NotNull final String jpql = "SELECT m FROM Task m WHERE user.id = :userId AND project.id = :projectId";
        return entityManager.createQuery(jpql, Task.class)
                .setParameter("userId", userId)
                .setParameter("projectId", projectId)
                .getResultList();
    }

    @NotNull
    @Override
    public List<Task> findAll() {
        @NotNull final String jpql = "SELECT m FROM Task m";
        return entityManager.createQuery(jpql, Task.class).getResultList();
    }

    @NotNull
    @Override
    public List<Task> findAll(@Nullable final String userId) {
        @NotNull final String jpql = "SELECT m FROM Task m WHERE user.id = :userId";
        return entityManager.createQuery(jpql, Task.class)
                .setParameter("userId", userId)
                .getResultList();
    }

    @Nullable
    @Override
    public List<Task> findAll(@Nullable final String userId, @Nullable final Sort sort) {
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Task> criteriaQuery = criteriaBuilder.createQuery(Task.class);
        Root<Task> task = criteriaQuery.from(Task.class);
        criteriaQuery.select(task)
                .where(criteriaBuilder.equal(task.get("userId"), userId))
                .orderBy(criteriaBuilder.asc(task.get(Sort.getOrderByField(sort))));
        TypedQuery<Task> typedQuery = entityManager.createQuery(criteriaQuery);
        return typedQuery.getResultList();
    }

    @Override
    public Task findOneById(@Nullable final String id) {
        return entityManager.find(Task.class, id);
    }

    @Override
    public Task findOneById(@Nullable final String userId, @Nullable final String id) {
        @NotNull final String jpql = "SELECT m FROM Task m WHERE id = :id AND user.id = :userId";
        return entityManager.createQuery(jpql, Task.class)
                .setParameter("id", id)
                .setParameter("userId", userId)
                .setMaxResults(1)
                .getResultList()
                .stream().findFirst().orElse(null);
    }

    @Override
    public boolean existsById(@Nullable final String id) {
        return findOneById(id) != null;
    }

    @Override
    public boolean existsById(@Nullable final String userId, @Nullable final String id) {
        return findOneById(userId, id) != null;
    }

    @Override
    public int getSize() {
        @NotNull final String jpql = "SELECT COUNT(m) FROM Task";
        return entityManager.createQuery(jpql, Task.class)
                .setMaxResults(1)
                .getFirstResult();
    }

    @Override
    public int getSize(@Nullable final String userId) {
        @NotNull final String jpql = "SELECT COUNT(m) FROM Task WHERE user.id = :userId";
        return entityManager.createQuery(jpql, Task.class)
                .setParameter("userId", userId)
                .setMaxResults(1)
                .getFirstResult();
    }

    @Override
    public void clear() {
        @Nullable final List<Task> tasks = findAll();
        if (!tasks.isEmpty())
            tasks.forEach(entityManager::remove);
    }

    @Override
    public void clear(@Nullable final String userId) {
        @NotNull final String jpql = "DELETE FROM Task m WHERE user.id = :userId";
        entityManager.createQuery(jpql)
                .setParameter("userId", userId)
                .executeUpdate();
    }

}
